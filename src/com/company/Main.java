package com.company;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class Main {

    public static void main(String[] args) throws IOException {
        String url = "https://www.nhk.or.jp/lesson/update/pdf/leall_en_t.pdf";
        String file = "jap_lang_book.pdf";

        downloadFileFromUrl(url, file);

        url = "http://www.perthtoparis.com/coffeetable/romania/blog/img-trans/tf12.jpg";
        file = "picture.jpg";

        downloadFileFromUrl(url, file);

    }

    private static void downloadFileFromUrl(String url, String file) {
        ReadableByteChannel readableBC = null;
        FileOutputStream fileOS = null;

        try {
            URL urlObj = new URL(url);
            readableBC = Channels.newChannel(urlObj.openStream());
            fileOS = new FileOutputStream(file);

            fileOS.getChannel().transferFrom(readableBC, 0, Long.MAX_VALUE);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileOS != null) {
                    fileOS.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (readableBC != null) {
                    readableBC.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
